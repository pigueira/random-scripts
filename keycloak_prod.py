import requests
import pprint
import sys
import os
from itostools.bundle import Bundle

bundle_name = sys.argv[1]
bundle = Bundle(bundle_name)

user = "ites"
user_itos_qa = "itos"

secret = os.getenv("CLIENT_SECRET_PROD")
secret_itos_qa = os.getenv("CLIENT_SECRET_PROD_ITOS")

token_endpoint = "https://auth.cern.ch/auth/realms/cern/api-access/token"
api = "https://authorization-service-api.web.cern.ch/api/v1.0"

token_ites = requests.post(token_endpoint, auth=(user, secret),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']

token_itos = requests.post(token_endpoint, auth=(user_itos_qa, secret_itos_qa),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']


headers_ites = {
    "Authorization": f"Bearer {token_ites}",
    "Content-Type": "application/json",
}

headers_itos = {
    "Authorization": f"Bearer {token_itos}",
    "Content-Type": "application/json",
}

for cluster in bundle.clusters.values():
    result = requests.delete(api + f"/Application/itos_{cluster.name}", headers=headers_itos).json()
    print("###########################################################################")
    print(cluster.name)

    # we get the application details
    result = requests.get(api + f"/Application/ites_{cluster.name}", headers=headers_ites).json()["data"]
    result["applicationIdentifier"] = f"itos_{cluster.name}"
    result["displayName"] = f"itos_{cluster.name}"
    result.pop("id")
    result.pop("identityId")
    result["managerId"] = "08dc8929-cb8d-40ab-83f3-a6e69169077e"

    # we create a new application copying everything except id + identifyId and changing the name
    result = requests.post(api + "/Application", headers=headers_itos, json=result).json()
    print(result)

    # we get the roles of the old application
    result = requests.get(api + f"/Application/ites_{cluster.name}/roles", headers=headers_ites).json()
    print(result)
    role_list = result["data"]

    if len(role_list) != 1:
        print("WARNING: Role list is not length one...")


    role = role_list[0]
    if role["name"] != f"es-{cluster.name}-kibana":
        print("Aborting: First role doesn't have expected name")
        sys.exit(1)

    role_id = role["id"]

    del role["id"]
    del role["applicationId"]
    role["name"] = "opensearch-dashboards-access"
    role["displayName"] = "opensearch-dashboards-access"

    groups = requests.get(api + f"/Application/ites_{cluster.name}/roles/{role_id}/groups", headers=headers_ites).json()
    print(groups)

    if result['pagination']['total'] > 1000:
        print("There are more than 1000 groups, group pagination is not implemented, aborting...")
        sys.exit(1)


    result = requests.post(api + f"/Application/itos_{cluster.name}/roles", headers=headers_itos, json=role).json()
    role_id = result["data"]["id"]

    for group in groups['data']:
        print(group)
        result = requests.post(api + f"/Application/itos_{cluster.name}/roles/{role_id}/groups/{group['id']}", headers=headers_itos).json()
        print(result)

    # we get the OpenID registration
    result = requests.get(api + f"/Registration/ites_{cluster.name}", headers=headers_ites).json()
    print(result)
    data = result["data"]
    # there is a special endpoint for the secret
    secret = requests.get(api + f"/Registration/ites_{cluster.name}/secret", headers=headers_ites).json()["data"]["secret"]

    for d in data:
        if "baseUrl" in d["registration"]:
            body = {
               "baseUrl": d["registration"]["baseUrl"],
               "redirectUris": d["registration"]["redirectUris"],
               "publicClient": d["registration"]["publicClient"],
               "implicitFlowEnabled": d["registration"]["serviceAccountsEnabled"],
               "serviceAccountsEnabled": d["registration"]["serviceAccountsEnabled"],
               "secret": secret,
               "attributes": d["registration"]["attributes"],
            }
        else:
            body = {
               "redirectUris": d["registration"]["redirectUris"],
               "publicClient": d["registration"]["publicClient"],
               "implicitFlowEnabled": d["registration"]["serviceAccountsEnabled"],
               "serviceAccountsEnabled": d["registration"]["serviceAccountsEnabled"],
               "secret": d["registration"]["secret"],
               "attributes": d["registration"]["attributes"],
            }


        # and we apply the same settings
        r = requests.post(api + f"/Registration/itos_{cluster.name}/f0000000-0000-0000-0000-000000000051", headers=headers_itos, json=body).text
        print(r)

