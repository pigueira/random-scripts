import requests
import pprint
import sys
import os
from itostools.bundle import Bundle

bundle_name = sys.argv[1]
bundle = Bundle(bundle_name)

user = "ites"

secret = os.getenv("CLIENT_SECRET_PROD")

token_endpoint = "https://auth.cern.ch/auth/realms/cern/api-access/token"
api = "https://authorization-service-api.web.cern.ch/api/v1.0"

token_ites = requests.post(token_endpoint, auth=(user, secret),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']


headers_ites = {
    "Authorization": f"Bearer {token_ites}",
    "Content-Type": "application/json",
}

for cluster in bundle.clusters.values():
    result = requests.delete(api + f"/Application/ites_{cluster.name}", headers=headers_ites).json()
    print(result.text)
