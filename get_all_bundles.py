from itostools.service import Service
from itostools.bundle import Bundle


s = Service()

for bundle_name in s.get_all_bundles_list():
    bundle = Bundle(bundle_name)
    print("Bundle: " + bundle_name)

    for cluster in bundle.clusters.values():
        print(f"\t{cluster.name}")

