import requests
import pprint
import sys
import os
from itostools.bundle import Bundle

bundle_name = sys.argv[1]
bundle = Bundle(bundle_name)

user = "ites-qa"
user_itos_qa = "itos-qa"

secret = os.getenv("CLIENT_SECRET_QA")
secret_itos_qa = os.getenv("CLIENT_SECRET_QA_ITOS_QA")

token_endpoint = "https://keycloak-qa.cern.ch/auth/realms/cern/api-access/token"
api = "https://authorization-service-api-qa.web.cern.ch/api/v1.0"

token_ites = requests.post(token_endpoint, auth=(user, secret),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']

token_itos = requests.post(token_endpoint, auth=(user_itos_qa, secret_itos_qa),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']


headers_ites = {
    "Authorization": f"Bearer {token_ites}",
    "Content-Type": "application/json",
}

headers_itos = {
    "Authorization": f"Bearer {token_itos}",
    "Content-Type": "application/json",
}

for cluster in bundle.clusters.values():
    if cluster.name != "pigtest3882":
        continue
    result = requests.delete(api + f"/Application/itos_{cluster.name}", headers=headers_itos)
    print("###########################################################################")
    print(cluster.name)

    # we get the application details
    result = requests.get(api + f"/Application/ites_{cluster.name}", headers=headers_ites).json()["data"]
    result["applicationIdentifier"] = f"itos_{cluster.name}"
    result["displayName"] = f"itos_{cluster.name}"
    result.pop("id")
    result.pop("identityId")
    result["managerId"] = "08dc8928-f70d-481d-8c23-5b10a4aaa340"

    # we create a new application copying everything except id + identifyId and changing the name
    result = requests.post(api + "/Application", headers=headers_itos, json=result).json()
    print(result)

    # we get the roles of the old application
    result = requests.get(api + f"/Application/ites_{cluster.name}/roles", headers=headers_ites).json()
    print(result)
    role_list = result["data"]

    if len(role_list) != 1:
        print("Role list is not length one, aborting...")
        sys.exit(1)

    role = role_list[0]
    role_id = role["id"]

    del role["id"]
    del role["applicationId"]
    role["name"] = "opensearch-dashboards-access"
    role["displayName"] = "opensearch-dashboards-access"

    groups = requests.get(api + f"/Application/ites_{cluster.name}/roles/{role_id}/groups", headers=headers_ites).json()
    print(groups)

    if result['pagination']['total'] > 1000:
        print("There are more than 1000 groups, group pagination is not implemented, aborting...")
        sys.exit(1)


    result = requests.post(api + f"/Application/itos_{cluster.name}/roles", headers=headers_itos, json=role).json()
    role_id = result["data"]["id"]

    for group in groups['data']:
        print(group)
        result = requests.post(api + f"/Application/itos_{cluster.name}/roles/{role_id}/groups/{group['id']}", headers=headers_itos).json()
        print(result)

    # we get the OpenID registration
    result = requests.get(api + f"/Registration/ites_{cluster.name}", headers=headers_ites).json()
    print(result)
    data = result["data"]

    for d in data:
        body = {
           "baseUrl": d["registration"]["baseUrl"],
           "redirectUris": d["registration"]["redirectUris"],
           "publicClient": d["registration"]["publicClient"],
           "implicitFlowEnabled": d["registration"]["serviceAccountsEnabled"],
           "serviceAccountsEnabled": d["registration"]["serviceAccountsEnabled"],
           "secret": d["registration"]["secret"],
           "attributes": d["registration"]["attributes"],
        }

        # and we apply the same settings
        r = requests.post(api + f"/Registration/itos_{cluster.name}/f0000000-0000-0000-0000-000000000051", headers=headers_itos, json=body).text
        print(r)

