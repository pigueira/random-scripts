import requests
import pprint
import sys
import os
from itostools.bundle import Bundle

bundle_name = sys.argv[1]
bundle = Bundle(bundle_name)

user = "ites"
user_itos_qa = "itos-qa"

#secret = os.getenv("CLIENT_SECRET_PROD")
secret_itos_qa = os.getenv("CLIENT_SECRET_DEV_ITOS")

token_endpoint = "https://keycloak-qa.cern.ch/auth/realms/cern/api-access/token"
api = "https://authorization-service-api-qa.web.cern.ch/api/v1.0"

#token_ites = requests.post(token_endpoint, auth=(user, secret),
#                      data={"audience": "authorization-service-api",
#                            "grant_type": "client_credentials"}).json()['access_token']

print(requests.post(token_endpoint, auth=(user_itos_qa, secret_itos_qa),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json())

token_itos = requests.post(token_endpoint, auth=(user_itos_qa, secret_itos_qa),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']


#headers_ites = {
#    "Authorization": f"Bearer {token_ites}",
#    "Content-Type": "application/json",
#}

headers_itos = {
    "Authorization": f"Bearer {token_itos}",
    "Content-Type": "application/json",
}

result = requests.delete(api + f"/Application/itos_pigtest3882", headers=headers_itos).json()["data"]

#for cluster in bundle.clusters.values():
#    print("###########################################################################")
#    print(cluster.name)
#
#    # we get the application details
#    result = requests.get(api + f"/Application/itos_{cluster.name}", headers=headers_ites).json()["data"]
#    print(result)
##
