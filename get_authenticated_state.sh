#!/bin/sh
cluster=$1

#/usr/bin/curl -k -X PATCH -u : --negotiate https://os-${cluster}.cern.ch/os/_opendistro/_security/api/audit -H "Content-Type: application/json" -d '[{"op":"add","path":"/config/audit/disabled_rest_categories","value":["GRANTED_PRIVILEGES"]},{"op":"add","path":"/config/audit/log_request_body","value":false},{"op":"add","path":"/config/audit/disabled_transport_categories","value":["AUTHENTICATED", "GRANTED_PRIVILEGES"]}]'
#/usr/bin/curl -k -X PATCH -u : --negotiate https://os-${cluster}.cern.ch/os/_opendistro/_security/api/audit -H "Content-Type: application/json" -d '[{"op":"add","path":"/config/audit/disabled_rest_categories","value":["AUTHENTICATED", "GRANTED_PRIVILEGES"]},{"op":"add","path":"/config/audit/log_request_body","value":false},{"op":"add","path":"/config/audit/disabled_transport_categories","value":["AUTHENTICATED", "GRANTED_PRIVILEGES"]}]'

/usr/bin/curl -s -k -X GET -u : --negotiate https://os-${cluster}.cern.ch/os/_opendistro/_security/api/audit -H "Content-Type: application/json" | jq '"\(.config.audit.disabled_rest_categories) \(.config.audit.disabled_transport_categories) \(.config.audit.log_request_body)"'
