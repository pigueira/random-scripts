import requests
import pprint
import sys
import os

app_name = sys.argv[1]

user = "ites-qa"
secret = os.getenv("CLIENT_SECRET_QA")
token_endpoint = "https://keycloak-qa.cern.ch/auth/realms/cern/api-access/token"
api = "https://authorization-service-api-qa.web.cern.ch/api/v1.0"

token = requests.post(token_endpoint, auth=(user, secret),
                      data={"audience": "authorization-service-api",
                            "grant_type": "client_credentials"}).json()['access_token']

headers = {
    "Authorization": f"Bearer {token}",
    "Content-Type": "application/json",
}

#result = requests.get(api + "/Application", headers=headers).json()
#json_result = result['data']
#
#while not result['pagination']['next'] is None:
#    url = api.replace('/api/v1.0', '') + result['pagination']['next']
#    result = requests.get(url, headers=headers).json()
#    json_result += result['data']
#
#for app in json_result:
#    if app["applicationIdentifier"] == app_name:
#        app_id_to_delete = 
#        break

result = requests.delete(api + f"/Application/{app_name}", headers=headers).json()
print(result)

#pigueira["applicationIdentifier"] = "itos_pigueira1"
#pigueira["displayName"] = "itos_pigueira1"
#pigueira.pop("id")
#pigueira.pop("identityId")
#result = requests.post(api + "/Application", headers=headers, json=pigueira).json()
#print(result)



#for app in json_result:
#        continue
#    if cluster_name in clusters:
#        clusters.remove(cluster_name)
#        print(cluster_name)
#        result = requests.get(api + f"/Registration/{app['id']}", headers=headers).json()
#        registrationId = result['data'][0]['registrationId']
#        uris = result['data'][0]['registration']['redirectUris']
#        old_uris = {'redirectUris': uris}
#        new_uris = {'redirectUris': uris + [f"https://os-{cluster_name}.cern.ch/*"]}
#        result = requests.put(api + f"/Registration/{registrationId}", json=new_uris, headers=headers)  
#        print(result.text)
#        print(f"Old: {old_uris}")
#        print(f"New: {new_uris}")
#
