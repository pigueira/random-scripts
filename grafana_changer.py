import requests
import json
import argparse

parser = argparse.ArgumentParser(description="Helper to bulk edit data sources")
parser.add_argument("--show-index-uids", action="store_true",
                    help="show index uids")
parser.add_argument("--old-ds", type=str, required=False,
                    help="the old data source")
parser.add_argument("--new-ds", type=str, required=False,
                    help="the new data source")
parser.add_argument("--token", type=str, required=True,
                    help="the grafana token")
parser.add_argument("--grafana-url", type=str, default="https://monit-grafana.cern.ch/",
                    help="the grafana url")

# Parse the arguments
args = parser.parse_args()

token = args.token
grafana_url = args.grafana_url

if args.show_index_uids:
    result = requests.get(f"{grafana_url}/api/datasources", headers={"Authorization": f"Bearer {token}"}).json()
    for r in result:
        print(r["name"], r["uid"])

if args.old_ds or args.new_ds:
    old_ds = args.old_ds
    new_ds = args.new_ds

    result = requests.get(f"{grafana_url}/api/search", headers={"Authorization": f"Bearer {token}"}).json()
    for r in result:
        result = requests.get(f"{grafana_url}/api/dashboards/uid/{r['uid']}", headers={"Authorization": f"Bearer {token}"}).text
        if old_ds in result:
            print(f"uid appears in: {r['title']}")
            if args.new_ds:
                print(f"Changing dashboard {r['title']} with new datasource")
                replaced = result.replace(old_ds, new_ds)
                new_json = json.loads(replaced)
                print(requests.post(f"{grafana_url}/api/dashboards/db", json=new_json, headers={"Authorization": f"Bearer {token}"}).text)
