#/bin/sh

date="$1"
until [[ $date > $2 ]]; do
    multi_msg=$(cat <<EOF
/usr/bin/curl -H "Content-Type: application/json" -XPOST -u : --negotiate https://os-atlas2.cern.ch/os/_reindex -d '{
  "source": {
    "index": "atlas_pandalogs-${date}"
  },
  "dest": {
    "index": "atlas_pandalogs-${date}new"
  }
}'
EOF
)
    echo "$multi_msg"
    date=$(date -I -d "$date + 1 day")
done
